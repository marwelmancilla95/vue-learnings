import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

export const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../views/HomeView.vue"),
  },
  {
    path: "/props",
    name: "props",
    component: () => import("../views/props/ParentView.vue"),
  },
  {
    path: "/forms",
    name: "forms",
    component: () => import("../views/forms/FormView.vue"),
  },
  {
    path: "/vuex",
    name: "vuex",
    component: () => import("../views/vuex/VuexView.vue"),
  },
  {
    path: "/lifecycle",
    name: "lifecycle",
    component: () => import("../views/lifecycle/LifeCyclesView.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
