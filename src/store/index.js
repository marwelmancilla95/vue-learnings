import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

const url = "https://icanhazdadjoke.com";
const headers = { Accept: "application/json" };

// FLOW
// components dispatch actions > actions commit mutations > mutation changes state

export default new Vuex.Store({
  state: {
    // state variables
    sampleStringState: "initial string state",
  },
  getters: {
    // get store state (similar to useSelector)
    getSampleStringState(state) {
      return state.sampleStringState;
    },
  },
  mutations: {
    // synchronous
    setSampleStringState(state, payload) {
      state.sampleStringState = payload;
    },
  },
  actions: {
    // asynchronous
    async setSampleStringState(state) {
      const fromAPI = await axios.get(url, { headers });
      state.commit("setSampleStringState", fromAPI.data.joke); // commit triggers mutations
    },
  },
  modules: {
    // similar to slices, Each module can contain its own state, mutations, actions, getters, and even nested modules
  },
});
